# 4DY3N
Sample project using Foursquare API.

## See it in action
http://4dy3n.s3-website-sa-east-1.amazonaws.com/

## Running locally
```js
yarn install
yarn start
```

### Starting Point
I used a boilerplate project that I have as the starting point for this project. Its a simple project that have most of the things you need for
a React Project. (Babel, React, Redux, Redux Saga, Webpack etc)

### CI/CD
I used Gitlab's CI/CD to deploy the application to a bucket on S3. You can see the configuration on the file `.gitlab-ci.yml`

### Google Cloud Function
I createad a cloud function to hide the Foursquare API credentials and cache the response.

### Materialize CSS
I used materialize-css to avoid Programmer Art

### Known Bug
The button near the search bar uses getCurrentPosition() and its deprecated on Chrome over http.

