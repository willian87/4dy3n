import React, { Component } from 'react';
import PropTypes from 'prop-types';
import M from 'materialize-css';

import './style.scss';

class Select extends Component {
  constructor(props) {
    super(props);
    this.selectRef = React.createRef();
  }

  componentDidMount() {
    M.FormSelect.init(this.selectRef.current);
  }

  render() {
    const { children, onChange } = this.props;
    return (
      <div className="Select">
        <select ref={this.selectRef} onChange={onChange}>
          {children}
        </select>
      </div>
    );
  }
}

Select.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element),
};

Select.defaultProps = {
  children: [],
};

export default Select;
