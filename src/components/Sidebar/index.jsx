import React from 'react';

import VenuesFilter from '../VenuesFilter';
import VenuesList from '../VenuesList';

import './style.scss';

const Sidebar = () => (
  <div className="Sidebar">
    <h1>4DY3N</h1>
    <VenuesFilter />
    <VenuesList />
  </div>
);

export default Sidebar;
