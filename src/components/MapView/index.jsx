import React from 'react';
import M from 'materialize-css';
import { connect } from 'react-redux';

import GoogleMap from '../GoogleMap';
import { searchVenues } from '../App/actions';

import './style.scss';
import { getLoading } from '../App/selectors';

const ENTER_KEY_CODE = 13;

class MapView extends React.Component {
  componentWillMount() {
    const { dispatch } = this.props;
    this.handleInputKeyDown = this.handleInputKeyDown.bind(this);
    this.handleLocationButtonClick = this.handleLocationButtonClick.bind(this);
    dispatch(searchVenues());
  }

  handleInputKeyDown(event) {
    const { dispatch } = this.props;
    if (event.keyCode === ENTER_KEY_CODE) {
      const near = event.currentTarget.value;
      dispatch(searchVenues({ near }));
    }
  }

  handleLocationButtonClick() {
    const { dispatch } = this.props;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const { coords } = position;
        dispatch(searchVenues({ ll: `${coords.latitude},${coords.longitude}` }));
      });
    } else {
      M.toast({ html: 'Geolocation not available' });
    }
  }

  render() {
    const { loading } = this.props;
    return (
      <section className="MapView">
        <section className="content">
          { loading && (
            <div className="progress">
              <div className="indeterminate" />
            </div>
          )
          }

          <div className="Header">
            <input type="search" placeholder="Where are you?" onKeyDown={this.handleInputKeyDown} />
            <button type="button" className="btn-floating btn-large waves-effect waves-light red" onClick={this.handleLocationButtonClick}>
              <i className="material-icons">place</i>
            </button>
          </div>
          <GoogleMap />
        </section>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  loading: getLoading(state),
});

export default connect(mapStateToProps)(MapView);
