import React from 'react';
import PropTypes from 'prop-types';

const MapInfoWindow = ({
  name, icon, address, link, category,
}) => (
  <div className="MapInfoWindow">
    <div className="title">
      <img className="icon" src={icon} alt={name} />
      <div className="desc">
        <div className="category">{category}</div>
        <div className="name">{name}</div>
      </div>
    </div>
    <div className="address">{address.map(item => <p key={item.split(' ').join('')}>{item}</p>)}</div>
    <a href={link} target="_blank" rel="noopener noreferrer">More info</a>
  </div>
);

MapInfoWindow.propTypes = {
  name: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  address: PropTypes.arrayOf(PropTypes.string).isRequired,
  link: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
};

export default MapInfoWindow;
