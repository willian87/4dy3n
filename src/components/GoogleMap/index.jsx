/* eslint-disable no-return-assign */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { GoogleApiWrapper } from 'google-maps-react';
import { connect } from 'react-redux';
import { renderToString } from 'react-dom/server';

import MapInfoWindow from './MapInfoWindow';
import { GOOGLE_MAPS_API_KEY, DEFAULT_MAP_POSITION } from '../App/config';
import { getVenues, getHighlightedMarker, getSelectedMarker } from '../App/selectors';

import './style.scss';

const googleMapsStyle = {
  width: '100%',
  height: '100%',
};

class MapView extends Component {
  constructor(props) {
    super(props);
    this.map = null;
    this.markers = [];
    this.infoWindow = null;
    this.mapContainerRef = React.createRef();
    this.handleMarkerClick = this.handleMarkerClick.bind(this);
  }

  componentDidMount() {
    this.initMap();
  }

  componentWillReceiveProps(nextProps) {
    const { venues, highlightedMarker, selectedMarker } = this.props;
    const {
      venues: newVenues,
      highlightedMarker: newHighlightedMarker,
      selectedMarker: newSelectedMarker,
    } = nextProps;
    if (JSON.stringify(venues) !== JSON.stringify(newVenues)) {
      this.updateMapMarkers(newVenues);
    }

    if (highlightedMarker !== newHighlightedMarker) {
      this.highlightMarker(newHighlightedMarker);
    }

    if (selectedMarker !== newSelectedMarker) {
      this.selectMarker(newSelectedMarker);
    }
  }

  getMarkerById(id) {
    for (let index = 0; index < this.markers.length; index++) {
      const marker = this.markers[index];
      if (marker.id === id) {
        return marker;
      }
    }
    return null;
  }

  updateMapMarkers(venues) {
    const { google } = this.props;
    this.removeAllMarkers();
    const bounds = new google.maps.LatLngBounds();
    venues.forEach((item) => {
      const markerInfo = {
        id: item.venue.id,
        name: item.venue.name,
        position: {
          lat: item.venue.location.lat,
          lng: item.venue.location.lng,
        },
        venue: item.venue,
        title: item.venue.name,
        map: this.map,
      };
      const marker = new google.maps.Marker(markerInfo);

      marker.addListener('click', () => this.handleMarkerClick(marker));

      this.markers.push(marker);
      bounds.extend(marker.position);
    });

    if (venues.length > 0) {
      this.map.fitBounds(bounds);
    }
    if (venues.length === 1) this.map.setZoom(15);
  }

  removeAllMarkers() {
    this.markers.forEach(marker => marker.setMap(null));
    this.markers.length = 0;
  }

  initMap() {
    const { google } = this.props;
    this.map = new google.maps.Map(
      this.mapContainerRef.current,
      { zoom: 13, center: DEFAULT_MAP_POSITION },
    );
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      const { venues } = this.props;
      this.updateMapMarkers(venues);
    });
  }

  highlightMarker(id) {
    const { google } = this.props;
    this.markers.forEach((marker) => {
      if (marker.id === id) {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      } else {
        marker.setAnimation(null);
      }
    });
  }

  selectMarker(id) {
    const marker = this.getMarkerById(id);
    this.map.setZoom(19);
    this.map.setCenter(marker.getPosition());
    this.handleMarkerClick(marker);
  }

  handleMarkerClick(marker) {
    const { google } = this.props;
    if (this.infoWindow !== null) {
      this.infoWindow.close();
    }
    const venue = marker.venue;
    const name = venue.name;
    const category = venue.categories[0];
    const icon = category.icon;
    const img = `${icon.prefix}bg_32${icon.suffix}`;
    const address = venue.location.formattedAddress;
    const link = venue.id === 'willcosta' ? 'https://www.linkedin.com/in/willcosta/' : `https://foursquare.com/v/${venue.id}`;

    this.infoWindow = new google.maps.InfoWindow({
      content: renderToString(
        <MapInfoWindow
          category={category.name}
          name={name}
          icon={img}
          address={address}
          link={link}
        />,
      ),
    });
    this.infoWindow.open(marker.get('map'), marker);
  }

  render() {
    return (
      <div className="GoogleMap">
        <div ref={this.mapContainerRef} className="map-container" style={googleMapsStyle} />
      </div>
    );
  }
}

MapView.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  google: PropTypes.object.isRequired,
  venues: PropTypes.arrayOf(PropTypes.object),
  highlightedMarker: PropTypes.string,
  selectedMarker: PropTypes.string,
};

MapView.defaultProps = {
  venues: [],
  highlightedMarker: null,
  selectedMarker: null,
};

const mapStateToProps = state => ({
  venues: getVenues(state),
  highlightedMarker: getHighlightedMarker(state),
  selectedMarker: getSelectedMarker(state),
});

const withReduxMapView = connect(mapStateToProps)(MapView);

export default GoogleApiWrapper({
  apiKey: GOOGLE_MAPS_API_KEY,
})(withReduxMapView);
