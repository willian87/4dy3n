export const getAppState = state => state.app;
export const getHighlightedMarker = state => state.app.highlightedMarker;
export const getLoading = state => state.app.loading;
export const getSelectedMarker = state => state.app.selectedMarker;
export const getVenues = (state) => {
  const groups = state.app.venuesInfo.groups;
  const venues = [];

  groups.forEach((group) => {
    group.items.forEach((item) => {
      venues.push(item);
    });
  });

  return venues;
};
