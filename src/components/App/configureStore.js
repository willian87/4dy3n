import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware, { END } from 'redux-saga';

import rootReducer from './rootReducer';
import rootSagas from './rootSagas';

export default function configureStore(initialState = {}) {
  const sagaMiddleware = createSagaMiddleware();

  const composedMiddlewares = composeWithDevTools(
    applyMiddleware(sagaMiddleware),
  );

  const store = createStore(rootReducer, initialState, composedMiddlewares);

  /* eslint-enable */
  store.runSagas = () => sagaMiddleware.run(rootSagas);

  store.close = () => {
    store.dispatch(END);
  };
  return store;
}
