export const SEARCH_VENUES = 'App/SEARCH_VENUES';
export const SEARCH_VENUES_FAILED = 'App/SEARCH_VENUES_FAILED';
export const SEARCH_VENUES_COMPLETE = 'App/SEARCH_VENUES_COMPLETE';

export const HIGHLIGHT_MAP_MARKER = 'App/HIGHLIGHT_MAP_MARKER';
export const SELECT_MAP_MARKER = 'App/SELECT_MAP_MARKER';

export const highlightMapMaker = id => ({ type: HIGHLIGHT_MAP_MARKER, id });
export const searchVenues = (params, filters) => ({ type: SEARCH_VENUES, params, filters });
export const selectMapMaker = id => ({ type: SELECT_MAP_MARKER, id });
