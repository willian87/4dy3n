import {
  SEARCH_VENUES,
  SEARCH_VENUES_FAILED,
  SEARCH_VENUES_COMPLETE,
  HIGHLIGHT_MAP_MARKER,
  SELECT_MAP_MARKER,
} from './actions';

const defaultState = {
  loading: true,
  error: false,
  venuesInfo: {
    suggestedBounds: null,
    groups: [],
  },
  lastSearchedPlace: null,
  highlightedMarker: null,
  selectedMarker: null,
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case SEARCH_VENUES:
      return {
        ...state,
        loading: true,
        venuesInfo: {
          ...state.venuesInfo,
          groups: [],
        },
      };

    case SEARCH_VENUES_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case SEARCH_VENUES_COMPLETE:
      return {
        ...state,
        loading: false,
        venuesInfo: action.venuesInfo,
        lastSearchedPlace: action.lastSearchedPlace,
      };

    case HIGHLIGHT_MAP_MARKER:
      return {
        ...state,
        highlightedMarker: action.id,
      };

    case SELECT_MAP_MARKER:
      return {
        ...state,
        selectedMarker: action.id,
      };

    default:
      return state;
  }
};

export default reducer;
