import Axios from 'axios';
import M from 'materialize-css';
import qs from 'query-string';
import { put, takeEvery } from 'redux-saga/effects';

import { BASE_API_URL } from './config';
import { SEARCH_VENUES, SEARCH_VENUES_COMPLETE, SEARCH_VENUES_FAILED } from './actions';

function* loadVenuesHandler(action) {
  const params = action.params || { near: 'Amsterdam' };
  const filters = action.filters || {};
  try {
    const queryString = { ...params, ...filters };
    const apiUrl = `${BASE_API_URL}venues/explore?${qs.stringify(queryString)}`;
    const response = yield Axios.get(apiUrl);
    yield put(
      {
        type: SEARCH_VENUES_COMPLETE,
        venuesInfo: response.data.response,
        lastSearchedPlace: params,
      },
    );
  } catch (e) {
    M.toast({ html: e.response.data.errorDetail });
    yield put({ type: SEARCH_VENUES_FAILED, error: e.response });
  }
}

export default [takeEvery(SEARCH_VENUES, loadVenuesHandler)];
