import React from 'react';
import { Provider } from 'react-redux';
import { hot } from 'react-hot-loader';

import Sidebar from '../Sidebar';
import MapView from '../MapView';

import './style.scss';

const App = ({ store }) => (
  <Provider store={store}>
    <div className="app">
      <Sidebar />
      <MapView />
    </div>
  </Provider>
);

export default hot(module)(App);
