import { all } from 'redux-saga/effects';
import AppSagas from './sagas';

export default function* rootSagas() {
  yield all([...AppSagas]);
}
