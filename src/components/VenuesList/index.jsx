import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getVenues } from '../App/selectors';
import { highlightMapMaker, selectMapMaker } from '../App/actions';

import './style.scss';

class VenuesList extends Component {
  constructor(props) {
    super(props);
    this.handleListItemClick = this.handleListItemClick.bind(this);
    this.handleListItemMouseOver = this.handleListItemMouseOver.bind(this);
    this.handleListItemMouseOut = this.handleListItemMouseOut.bind(this);
  }

  handleListItemClick(event) {
    const { dispatch } = this.props;
    const id = event.currentTarget.dataset.id;
    dispatch(selectMapMaker(id));
  }

  handleListItemMouseOver(event) {
    const { dispatch } = this.props;
    const id = event.currentTarget.dataset.id;
    dispatch(highlightMapMaker(id));
  }

  handleListItemMouseOut() {
    const { dispatch } = this.props;
    dispatch(highlightMapMaker(null));
  }

  render() {
    const { venues } = this.props;
    return (
      <section className="VenuesList">
        <div className="title">Found {venues.length}</div>
        <ul className="venues collection">
          { venues.map((item) => {
            const { venue } = item;
            const { id, name, categories } = venue;
            const category = categories[0];
            const icon = category.icon;
            const img = `${icon.prefix}bg_32${icon.suffix}`;
            return (
              <a
                key={id}
                data-id={id}
                className="collection-item"
                href="#!"
                onClick={this.handleListItemClick}
                onMouseEnter={this.handleListItemMouseOver}
                onFocus={this.handleListItemMouseOver}
                onMouseLeave={this.handleListItemMouseOut}
                onBlur={this.handleListItemMouseOut}
              >
                <div className="icon">
                  <img src={img} alt={name} />
                </div>
                <div className="desc">
                  <span className="category">{category.name}</span>
                  <span className="label">{name}</span>
                </div>
              </a>
            );
          })}
        </ul>
      </section>
    );
  }
}

VenuesList.propTypes = {
  venues: PropTypes.arrayOf(PropTypes.object),
};

VenuesList.defaultProps = {
  venues: [],
};

const mapStateToProps = state => ({
  venues: getVenues(state),
});

export default connect(mapStateToProps)(VenuesList);
