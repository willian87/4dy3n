import React, { Component } from 'react';
import { connect } from 'react-redux';

import Select from '../Select';
import { getAppState } from '../App/selectors';
import { searchVenues } from '../App/actions';

import './style.scss';

class VenuesFilter extends Component {
  constructor(props) {
    super(props);
    this.handleSectionChange = this.handleSectionChange.bind(this);
  }

  handleSectionChange(event) {
    const { dispatch, appState } = this.props;
    const value = event.currentTarget.value;
    dispatch(searchVenues(appState.lastSearchedPlace, { section: value }));
  }

  render() {
    return (
      <section className="VenuesFilter">
        <div className="title">What are you looking for?</div>
        <div className="filters">
          <Select onChange={this.handleSectionChange}>
            <option>Any</option>
            <option value="food">Food</option>
            <option value="drinks">Drinks</option>
            <option value="coffee">Coffee</option>
            <option value="js-developer">Javascript / Front End Developer</option>
            <option value="shops">Shops</option>
            <option value="arts">Arts</option>
            <option value="outdoors">Outdoors</option>
            <option value="sights">Sights</option>
            <option value="trending">Trending</option>
          </Select>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  appState: getAppState(state),
});

export default connect(mapStateToProps)(VenuesFilter);
