module.exports = {
    "parser": "babel-eslint",
    "extends": ["airbnb"],
    "env": {
	   "browser": true,
	   "node": true,
       "jquery": true
	  },
    "rules": {
        "import/extensions": ["off", "never"],
        "no-bitwise": ["error", { "allow": ["~"] }],
        "no-plusplus": ["error", { "allowForLoopAfterthoughts": true }],
        "prefer-destructuring": [0],
        "react/jsx-one-expression-per-line": [0],
        "react/prop-types": [1, { ignore: ["dispatch", "match", "store"]}],
    },

    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
};
